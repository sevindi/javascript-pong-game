(function () {
    let CSS = {
        arena: {
            width: 900,
            height: 600,
            background: '#62247B',
            position: 'fixed',
            top: '50%',
            left: '50%',
            zIndex: '999',
            transform: 'translate(-50%, -50%)',
            overflow: 'hidden',
            userSelect: 'none',
        },
        ball: {
            width: 15,
            height: 15,
            position: 'absolute',
            top: 300,
            left: 450,
            borderRadius: 50,
            background: '#C6A62F',
            visibility: 'visible',
        },
        line: {
            width: 0,
            height: 600,
            borderLeft: '2px dashed #C6A62F',
            position: 'absolute',
            top: 0,
            left: '50%',
        },
        paddle: {
            width: 12,
            height: 85,
            position: 'absolute',
            background: '#C6A62F',
        },
        paddleLeft: {
            left: 0,
            top: 257.5,
        },
        paddleRight: {
            left: 888,
            top: 257.5,
        },
        scoreboard: {
            width: '50%',
            height: '20%',
            margin: 'auto',
            textAlign: 'center',
            fontSize: '400%',
            color: '#C6A62F',
        },
        scoreLeft: {
            float: 'left',
        },
        scoreRight: {
            float: 'right',
        },
        win: {
            display: 'absolute',
            fontSize: '400%',
            textAlign: 'center',
            width: '100%',
            marginTop: '15%',
            color: '#8bbc80',
            textShadow: '0 0 30px #91d5a0',
        },
    };

    let CONSTS = {
        gameSpeed: 15,
        scoreLeft: 0,
        scoreRight: 0,
        paddleSpeed: 5,
        paddleLeftSpeed: 0,
        paddleRightSpeed: 0,
        ballSpeedX: 0,
        ballSpeedY: 0,
    };

    function start() {
        const continueGame = loadFromLocalStorage();
        drawGame();
        if (!continueGame) {
            startBall();
        }
    }

    function drawGame() {
        $('<div/>', { id: 'pong-game' }).css(CSS.arena).appendTo('body');
        $('<div/>', { id: 'pong-line' }).css(CSS.line).appendTo('#pong-game');
        $('<div/>', { id: 'pong-ball' }).css(CSS.ball).appendTo('#pong-game');
        $('<div/>', { id: 'paddle-left' })
            .css($.extend(CSS.paddleLeft, CSS.paddle))
            .appendTo('#pong-game');
        $('<div/>', { id: 'paddle-right' })
            .css($.extend(CSS.paddleRight, CSS.paddle))
            .appendTo('#pong-game');
        $('<div/>', { id: 'scoreboard' })
            .css(CSS.scoreboard)
            .appendTo('#pong-game');
        $('<div/>', { id: 'score-left' })
            .css(CSS.scoreLeft)
            .html(CONSTS.scoreLeft)
            .appendTo('#scoreboard');
        $('<div/>', { id: 'score-right' })
            .css(CSS.scoreRight)
            .html(CONSTS.scoreRight)
            .appendTo('#scoreboard');
        $('<div/>', { id: 'win' }).css(CSS.win).appendTo('#pong-game');
    }

    // track key strokes to able to press more than one key at a time
    const keystore = [];
    onkeydown = function (val) {
        keystore[val.key] = val.type === 'keydown';
    };
    onkeyup = function (val) {
        keystore[val.key] = val.type === 'keydown';
    };

    function paddleMovement() {
        if (keystore.w) {
            if (CSS.paddleLeft.top >= 0) {
                CONSTS.paddleLeftSpeed = -CONSTS.paddleSpeed;
            } else CSS.paddleLeft.top = 0;
        } else if (keystore.w || keystore.s === false) {
            CONSTS.paddleLeftSpeed = 0;
        }
        if (keystore.s) {
            if (CSS.paddleLeft.top <= CSS.arena.height - CSS.paddle.height) {
                CONSTS.paddleLeftSpeed = CONSTS.paddleSpeed;
            } else CSS.paddleLeft.top = CSS.arena.height - CSS.paddle.height;
        } else if (keystore.s || keystore.w === false) {
            CONSTS.paddleLeftSpeed = 0;
        }

        if (keystore.ArrowDown) {
            if (CSS.paddleRight.top <= CSS.arena.height - CSS.paddle.height) {
                CONSTS.paddleRightSpeed = CONSTS.paddleSpeed;
            } else CSS.paddleRight.top = CSS.arena.height - CSS.paddle.height;
        } else if (keystore.ArrowDown || keystore.ArrowUp === false) {
            CONSTS.paddleRightSpeed = 0;
        }

        if (keystore.ArrowUp) {
            if (CSS.paddleRight.top >= 0) {
                CONSTS.paddleRightSpeed = -CONSTS.paddleSpeed;
            } else CSS.paddleRight.top = 0;
        } else if (keystore.ArrowUp || keystore.ArrowDown === false) {
            CONSTS.paddleRightSpeed = 0;
        }

        CSS.paddleLeft.top += CONSTS.paddleLeftSpeed;
        $('#paddle-left').css('top', CSS.paddleLeft.top);

        CSS.paddleRight.top += CONSTS.paddleRightSpeed;
        $('#paddle-right').css('top', CSS.paddleRight.top);
    }

    function ballMovement() {
        CSS.ball.top += CONSTS.ballSpeedY;
        CSS.ball.left += CONSTS.ballSpeedX;

        // ball bounce from upper and lower borders
        if (
            CSS.ball.top <= 0 ||
            CSS.ball.top >= CSS.arena.height - CSS.ball.height
        ) {
            CONSTS.ballSpeedY *= -1;
            CONSTS.ballSpeedX *= 1;
        }

        $('#pong-ball').css({ top: CSS.ball.top, left: CSS.ball.left });

        // ball bounce from left paddle or right player scores
        if (CSS.ball.left <= CSS.paddle.width) {
            if (
                CSS.ball.top + CSS.ball.height > CSS.paddleLeft.top &&
                CSS.ball.top < CSS.paddleLeft.top + CSS.paddle.height &&
                CONSTS.ballSpeedX < 0
            ) {
                CONSTS.ballSpeedX *= -1;
            } else if (CSS.ball.left < 0) {
                scored('right');
            }
        }

        // ball bounce from right paddle or left player scores
        if (
            CSS.ball.left >=
            CSS.arena.width - CSS.ball.width - CSS.paddle.width
        ) {
            if (
                CSS.ball.top + CSS.ball.height > CSS.paddleRight.top &&
                CSS.ball.top < CSS.paddleRight.top + CSS.paddle.height &&
                CONSTS.ballSpeedX > 0
            ) {
                CONSTS.ballSpeedX *= -1;
            } else if (CSS.ball.left > CSS.arena.width - CSS.ball.width) {
                scored('left');
            }
        }
    }

    function scored(scorer) {
        if (scorer === 'left') {
            CONSTS.scoreLeft += 1;
            $('#score-left').html(CONSTS.scoreLeft);
            if (CONSTS.scoreLeft >= 5) {
                $('#win').html('Left Player Wins!');
                endGame();
            } else {
                startBall();
            }
        }
        if (scorer === 'right') {
            CONSTS.scoreRight += 1;
            $('#score-right').html(CONSTS.scoreRight);
            if (CONSTS.scoreRight >= 5) {
                $('#win').html('Right Player Wins!');
                endGame();
            } else {
                startBall();
            }
        }
    }

    function startBall() {
        CSS.ball.top = 300;
        CSS.ball.left = 450;

        const side = Math.random() < 0.5 ? 1 : -1;
        const angle = Math.random() * (Math.PI * 2 / 3) + (Math.PI / 6);

        CONSTS.ballSpeedX = 6 * side * Math.sin(angle);
        CONSTS.ballSpeedY = 6 * Math.cos(angle);
    }

    function endGame() {
        clearInterval(gameInterval);

        localStorage.clear();

        CSS.ball.visibility = 'hidden';
        $('#pong-ball').css({ visibility: CSS.ball.visibility });
    }

    function saveToLocalStorage() {
        localStorage.setItem('css', JSON.stringify(CSS));
        localStorage.setItem('consts', JSON.stringify(CONSTS));
    }

    function loadFromLocalStorage() {
        const constsData = localStorage.getItem('consts');
        const cssData = localStorage.getItem('css');
        if (constsData != null && cssData != null) {
            CONSTS = JSON.parse(constsData);
            CSS = JSON.parse(cssData);
            return true;
        }
        return false;
    }

    const gameInterval = setInterval(() => {
        saveToLocalStorage();
        ballMovement();
        paddleMovement();
    }, CONSTS.gameSpeed);

    start();
})();
